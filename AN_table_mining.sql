-- @block Set Global Permissions
Set Global local_infile = 1;

-- @block Use database
USE dbo;

-- @block Create table
create table dbo.PUBACC_MW
(
      record_type               char(2)              not null,
      unique_system_identifier  numeric(9,0)         not null,
      uls_file_number           char(14)             null,
      ebf_number                varchar(30)          null,
      call_sign                 char(10)             null,
      pack_indicator            char(1)              null,
      pack_registration_num     int                  null,
      pack_name                 varchar(50)          null,
      type_of_operation         varchar(45)          null,
      smsa_code                 char(6)              null,
      station_class             char(4)              null,
      cum_effect_is_major       char(1)              null,
      status_code		char(1)		     null,
      status_date		datetime	     null
)


-- @block Load data from .dat file
LOAD DATA
	LOCAL
    INFILE "E:/FCC Weeklies/l_micro/MW.dat"
    IGNORE
    INTO TABLE dbo.PUBACC_MW
    FIELDS TERMINATED BY '|' 
    LINES TERMINATED BY '\n';

-- @block View head of data
SELECT * FROM pubacc_MW LIMIT 10;

-- @block Looking at the data, it seems that these first few rows of these columns are empty -- are the entire columns empty?
SELECT SUM(uls_file_number), SUM(ebf_number), SUM(antenna_action_performed),
 SUM(receive_zone_code), COUNT(NULLIF(height_to_tip, 0.0)), COUNT(NULLIF(tilt, 0.0)),
  COUNT(NULLIF(diversity_height, 0)), COUNT(NULLIF(diversity_gain, 0)), COUNT(NULLIF(diversity_beam, 0)),
   COUNT(NULLIF(reflector_height, 0)), COUNT(NULLIF(reflector_width, 0)), COUNT(NULLIF(reflector_separation, 0)),
    COUNT(NULLIF(repeater_seq_num, 0)), COUNT(NULLIF(back_to_back_rx_dish_gain, 0)), COUNT(NULLIF(back_to_back_tx_dish_gain, 0)),
     SUM(passive_repeater_id), SUM(alternative_cgsa_method), SUM(line_loss), SUM(status_code), COUNT(NULLIF(status_date, "0000-00-00 00:00:00")),
      SUM(psd_nonpsd_methodology), COUNT(NULLIF(maximum_erp, 0.000)) FROM pubacc_an;


-- @block Drop empty columns found from above query
ALTER TABLE pubacc_an
DROP COLUMN uls_file_number,
DROP COLUMN ebf_number,
DROP COLUMN antenna_action_performed,
DROP COLUMN receive_zone_code,
DROP height_to_tip,
DROP status_code,
DROP status_date,
DROP psd_nonpsd_methodology,
DROP maximum_erp;


-- @block Look at table again
SELECT * FROM pubacc_an LIMIT 10;
