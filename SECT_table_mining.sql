-- @block Set Global Permissions
Set Global local_infile = 1;

-- @block Use database
USE dbo;

-- @block oops
DROP TABLE dbo.SEC_BD

-- @block Create table
create table dbo.SEC_BD
(
	cik_number VARCHAR(10) null,
	company_name VARCHAR(60) null,
	reporting_file_number VARCHAR(25) null,
	address_one VARCHAR(40) null,
	address_two VARCHAR(40) null,
	city VARCHAR(30) null,
	state_code VARCHAR(2) null,
	zip_code VARCHAR(10) null
      )

-- @block Load data from .dat file
LOAD DATA
	LOCAL
    INFILE "E:/FCC Weeklies/SEC102022.csv"
    IGNORE
    INTO TABLE dbo.SEC_BD
    FIELDS TERMINATED BY ',' 
	OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\n';

-- @block View head of data
SELECT * FROM SEC_BD LIMIT 100;
