-- @block Look at the table
SELECT * FROM pubacc_en LIMIT 10;


-- @block Sickness
SELECT * FROM pubacc_en WHERE state = 'IL' LIMIT 10;



-- @block Join data from Antennas table

SELECT entity.call_sign, entity.entity_name, entity.city, entity.state, antenna.antenna_number, antenna.location_name, antenna.path_number
FROM pubacc_en entity JOIN pubacc_an antenna ON (entity.call_sign = antenna.call_sign)
WHERE entity.state IN ('IL', 'IN', 'OH', 'PA', 'WV', 'KY', 'MO') 
AND entity.entity_name IN
   ("AlarmNet, Inc",
    "Transmission Holdings, Inc.",
    "BNSF Railway Co.",
    "COMMONWEALTH EDISON COMPANY",
    "MPX, Inc",
    "Illinois Central Railroad Company",
    "Eastern MLG LLC",
    "Jefferson Microwave, LLC",
    "New Line Networks",
    "Webline Holdings LLC",
    "GTT Americas LLC",
    "EG Broadcast Newco Corp",
    "National Tower Company LLC",
    "Wireless Internetwork, LLC",
    "DC2A LLC",
    "Geodesic Networks LLC",
    "Blueline Comm",
    "Argos Engineering, LLC",
    "xWave Engineering LLC",
    "North Central Tower Co, LLC",
    "NeXXCom Wireless LLC",
    "Fundamental Broadcasting LLC",
    "AQ2AT LLC",
    "World Class Wireless, LLC",
    "Pierce Broadband, LLC",
    "Spectrum Holding Company LLC",
    "SW Networks",
    "iSignal",
    "Newgig networks") 