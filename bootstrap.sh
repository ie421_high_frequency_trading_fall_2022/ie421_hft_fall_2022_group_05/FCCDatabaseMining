# Update Packages
apt-get update
# Upgrade Packages
apt-get upgrade

# Download Git
apt-get install -y git 

# Set MYSQL Password
debconf-set-selections <<< 'mysql-server mysql-server/root_password password Password123'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password Password123'

# Install MySQL
apt-get install -y mysql-server

