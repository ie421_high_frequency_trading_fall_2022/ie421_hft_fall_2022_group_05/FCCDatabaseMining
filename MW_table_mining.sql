-- @block Set Global Permissions
Set Global local_infile = 1;

-- @block Use database
USE dbo;

-- @block Create table
create table dbo.PUBACC_AN
(
      record_type              	char(2)              null,
      unique_system_identifier  numeric(9,0)         not null,
      uls_file_number           char(14)             null,
      ebf_number                varchar(30)          null,
      call_sign                	char(10)             null,
      antenna_action_performed  char(1)              null,
      antenna_number            int                  null,
      location_number           int                  null,
      receive_zone_code         char(6)              null,
      antenna_type_code         char(1)              null,
      height_to_tip            	numeric(5,1)         null,
      height_to_center_raat     numeric(5,1)         null,
      antenna_make              varchar(25)          null,
      antenna_model             varchar(25)          null,
      tilt                      numeric(3,1)         null,
      polarization_code         char(5)              null,
      beamwidth                 numeric(4,1)         null,
      gain                      numeric(4,1)         null,
      azimuth                   numeric(4,1)         null,
      height_above_avg_terrain  numeric(5,1)         null,
      diversity_height          numeric(5,1)         null,
      diversity_gain            numeric(4,1)         null,
      diversity_beam            numeric(4,1)         null,
      reflector_height          numeric(5,1)         null,
      reflector_width           numeric(4,1)         null,
      reflector_separation      numeric(5,1)         null,
      repeater_seq_num          int                  null,
      back_to_back_tx_dish_gain numeric(4,1)         null,
      back_to_back_rx_dish_gain numeric(4,1)         null,
      location_name             varchar(20)          null,
      passive_repeater_id       int                  null,
      alternative_cgsa_method   char(1)              null,
      path_number               int                  null,
      line_loss                 numeric(3,1)         null,
      status_code		char(1)		     null,
      status_date		datetime	     null,
      psd_nonpsd_methodology    varchar(10)          null,
      maximum_erp               numeric(15,3)        null
)


-- @block Load data from .dat file
LOAD DATA
	LOCAL
    INFILE "E:/FCC Weeklies/l_micro/AN.dat"
    IGNORE
    INTO TABLE dbo.PUBACC_AN
    FIELDS TERMINATED BY '|' 
    LINES TERMINATED BY '\n';

-- @block View head of data
SELECT * FROM pubacc_an LIMIT 10;

-- @block Looking at the data, it seems that these first few rows of these columns are empty -- are the entire columns empty?
SELECT SUM(uls_file_number), SUM(ebf_number), SUM(antenna_action_performed),
 SUM(receive_zone_code), COUNT(NULLIF(height_to_tip, 0.0)), COUNT(NULLIF(tilt, 0.0)),
  COUNT(NULLIF(diversity_height, 0)), COUNT(NULLIF(diversity_gain, 0)), COUNT(NULLIF(diversity_beam, 0)),
   COUNT(NULLIF(reflector_height, 0)), COUNT(NULLIF(reflector_width, 0)), COUNT(NULLIF(reflector_separation, 0)),
    COUNT(NULLIF(repeater_seq_num, 0)), COUNT(NULLIF(back_to_back_rx_dish_gain, 0)), COUNT(NULLIF(back_to_back_tx_dish_gain, 0)),
     SUM(passive_repeater_id), SUM(alternative_cgsa_method), SUM(line_loss), SUM(status_code), COUNT(NULLIF(status_date, "0000-00-00 00:00:00")),
      SUM(psd_nonpsd_methodology), COUNT(NULLIF(maximum_erp, 0.000)) FROM pubacc_an;


-- @block Drop empty columns found from above query
ALTER TABLE pubacc_an
DROP COLUMN uls_file_number,
DROP COLUMN ebf_number,
DROP COLUMN antenna_action_performed,
DROP COLUMN receive_zone_code,
DROP height_to_tip,
DROP status_code,
DROP status_date,
DROP psd_nonpsd_methodology,
DROP maximum_erp;


-- @block Look at table again
SELECT * FROM pubacc_an LIMIT 10;
